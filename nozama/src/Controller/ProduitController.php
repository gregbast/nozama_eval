<?php

namespace App\Controller;

use App\Repository\ProduitsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use EasyCorp\Bundle\EasyAdminBundle\Contracts\Orm\EntityPaginatorInterface;

class ProduitController extends AbstractController
{
    #[Route('/produit', name: 'app_produit')]
    public function index(Request $request, ProduitsRepository $repository,PaginatorInterface $paginator): Response
    {
        $produits = $repository->findAll();
        $produits = $paginator->paginate(
        $produits,
        $request->query->getInt('page', 1),
            limit:9
        );
        return $this->render('pages/produit/index.html.twig', [
            'produits' => $produits
        ]);
    }
}
