-- phpMyAdmin SQL Dump
-- version 5.1.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 29, 2022 at 04:00 PM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nozama_eval`
--

-- --------------------------------------------------------

--
-- Table structure for table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220427123709', '2022-04-27 14:37:28', 67),
('DoctrineMigrations\\Version20220428073559', '2022-04-28 09:36:16', 133),
('DoctrineMigrations\\Version20220429065629', '2022-04-29 08:56:48', 5235);

-- --------------------------------------------------------

--
-- Table structure for table `messenger_messages`
--

CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `available_at` datetime NOT NULL,
  `delivered_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

CREATE TABLE `produits` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(5,2) DEFAULT NULL,
  `stock` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produits`
--

INSERT INTO `produits` (`id`, `name`, `description`, `image`, `price`, `stock`) VALUES
(2, 'tv', 'super', 'https://images.unsplash.com/photo-1612719181404-b9e6ea624954?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjB8fHRlbGV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '12.00', 2),
(3, 'billard', 'blabla', 'https://images.unsplash.com/photo-1580189975011-3e2e5ba11ef9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8YmlsbGFyZHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60', '23.00', 1),
(4, 'phone', 'pas mal', 'https://images.unsplash.com/photo-1525598912003-663126343e1f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvbmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '11.00', 1),
(5, 'voiture', 'et oui', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '33.00', 22),
(6, 'fqfsqsf', 'qsfqsfsssssssssss', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '11.00', 22),
(7, 'asasaszdaefae', 'azefaefdazfzfazfa', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '1.00', 1),
(8, 'saas', 'assasasa', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '11.00', 11),
(9, 'daczdfz', 'qaeeeeeeeeeeeeeeeeeeeeee', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '11.00', 1),
(10, 'AAAA', 'Zzzzzzzzzzzzzzzzzzzz', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '11.00', 4),
(11, 'gzkeu', 'gzegzgezegzetzrerhehe', 'https://images.unsplash.com/photo-1610578638373-6963803f0614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTR8fHZvaXR1cmV8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60', '2.00', 38),
(12, 'sasasas', 'asasasa', 'https://images.unsplash.com/photo-1651052003819-ec2abaa445f7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80', '1.00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(1, 'vetement'),
(2, 'Automobile'),
(3, 'Sport et loisir'),
(4, 'Informatique');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `firstname`, `lastname`) VALUES
(1, 'gregory.bastard73@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$T6rRpf.TQt02RLpwNxIgTezcC2mccxSjiMlhkLQidHDk8TyCv/UoS', 'greg', 'bastard'),
(2, 'billibob@gmail.com', '[]', '$2y$13$zJO.Ois3ojT.m0Sll1NV0O4sDVEIxFEzPt5Af.QJB3XquFBI23joa', 'bob', 'bill'),
(3, 'zipzap@gmail.com', '[]', '$2y$13$0CfWEm99JlrSYBIny3TWdOuOaBpfn60RMKQLGaTwT3aMd5s8Ayw9C', 'zip', 'zap'),
(4, 'gaspar@gmail.com', '[]', '$2y$13$LS7UDTCR.iWSIBOQJv1.FeNLI8C613EafurjF9m/Z7Fwl0VibJinG', 'gaspar', 'pargas'),
(5, 'asasas@gmail.com', '[]', '$2y$13$BdmfCFF23mEFmrgeHSMuzuQr2bIPkaiVsgIgZYukgokLIxel4qPYe', 'edd', 'zz'),
(7, 'rapard@simplon.co', '[\"ROLE_ADMIN\"]', '$2y$13$uGh88ps78JHZ5vTf5WEjKeRRHf6ua36rk97HPeg9a9b3rDj6vJNFm', 'raph', 'raph');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `messenger_messages`
--
ALTER TABLE `messenger_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  ADD KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  ADD KEY `IDX_75EA56E016BA31DB` (`delivered_at`);

--
-- Indexes for table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messenger_messages`
--
ALTER TABLE `messenger_messages`
  MODIFY `id` bigint NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `produits`
--
ALTER TABLE `produits`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
